package com.location.tracker;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.location.tracker.entity.Device;
import com.location.tracker.entity.TrackedDevice;
import com.location.tracker.entity.TrackingDevice;
import com.location.tracker.resource.DeviceResourceImp;
import com.location.tracker.resource.baseResource.DeviceResource;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DevicesActivity extends AppCompatActivity {

    private ListView lvDevices;
    private List<TrackedDevice> devices;
    private EditText etTrackingID;
    private EditText etTrackingName;
    private TrackingDevice trackingDevice;
    private DeviceResource deviceResource = MainActivity.deviceResource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devices);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        trackingDevice = new TrackingDevice();
        trackingDevice.setId(getDeviceID());

        lvDevices = (ListView) findViewById(R.id.listDevices);
        devices = new ArrayList<>();

        initializeList();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showActionOptionButtons();
            }
        });
    }

    private void showActionOptionButtons() {
        // create an alert builder
        final AlertDialog.Builder builder = new AlertDialog.Builder(DevicesActivity.this);

        // set the custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.devices_activity_buttons, null);

        Button btnAddDevice = (Button) customLayout.findViewById(R.id.btn_add_device);
        Button btnViewDevicesLocations = (Button) customLayout.findViewById(R.id.btn_view_devices_locations);

        builder.setView(customLayout);
        final AlertDialog dialog = builder.create();

        btnViewDevicesLocations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { goToTrackingActivity(getDeviceID());
                Intent intent = new Intent(getApplicationContext(), MapsTrackinngActivity.class);
                startActivity(intent);
                dialog.cancel();
            }
        });

        btnAddDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialogAddDevice();
                dialog.cancel();
            }
        });

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private boolean addDevice(TrackingDevice trackingDevice, TrackedDevice device){
        deviceResource.addTrackingDevice(trackingDevice, device);

        return true;
    }

    private void showAlertDialogAddDevice() {

        // create an alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //builder.setTitle("Device Details");

        // set the custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.device_dialog_view, null);
        etTrackingID = (EditText) customLayout.findViewById(R.id.etNewDeviceID);
        etTrackingName = (EditText) customLayout.findViewById(R.id.etNewDeviceName);

        builder.setView(customLayout);
        // add a button
        builder.setPositiveButton("Add/update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String trackingID = etTrackingID.getText().toString();
                String name = etTrackingName.getText().toString();
                TrackedDevice device = new TrackedDevice();
                device.setId(trackingID);
                device.setId("60891010d4d9eccc");
                device.setName(name);

                boolean added = addDevice(trackingDevice, device);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public String getDeviceID() {
        String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        return deviceId;
    }
    // do something with the data coming from the AlertDialog
    private void sendDialogDataToActivity(String data) {
        Toast.makeText(this, data, Toast.LENGTH_SHORT).show();
    }

    private void initializeList(){

        getDevices(getDeviceID());

        Device device = new Device();

        lvDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                Toast.makeText(getApplicationContext(),"Device " + position, Toast.LENGTH_LONG).show();
                //Remove selected device
                showRemoveDevicePopup();
            }
        });
    }

    private void getDevices(final String deviceID) {

        final TrackingDevice[] trackingDevice = {new TrackingDevice()};
        new DeviceResourceImp().trackingDeviceTable.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> snapshotIterator = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = snapshotIterator.iterator();

                while((iterator.hasNext())){

                    trackingDevice[0] = iterator.next().getValue(TrackingDevice.class);
                    if(trackingDevice[0].getId().equals(deviceID)){
                        devices = trackingDevice[0].getDevices();
                    }
                }

                if(trackingDevice[0].getDevices() != null){
                    MyDeviceList adapter = new MyDeviceList(trackingDevice[0], DevicesActivity.this);

                    lvDevices.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void showRemoveDevicePopup(){

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.devices_menu, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_show_devices) {
//            goToTrackingActivity(getDeviceID());
//            Intent intent = new Intent(getApplicationContext(), MapsTrackinngActivity.class);
//            startActivity(intent);
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    private void goToTrackingActivity(final String trackingDeviceId) {
        final DatabaseReference trackingDeviceTable = new DeviceResourceImp().trackingDeviceTable;
        trackingDeviceTable.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> snapshotIterator = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = snapshotIterator.iterator();

                while ((iterator.hasNext())) {

                    TrackingDevice trackingDevice = iterator.next().getValue(TrackingDevice.class);
                    if (trackingDevice.getId().equals(trackingDeviceId)) {
                        navigateToTrackingActivity(trackingDevice);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void navigateToTrackingActivity(TrackingDevice trackingDevice) {
        Intent intent = new Intent(getApplicationContext(), MapsTrackinngActivity.class);
        intent.putExtra("trackingDevice", trackingDevice);
        startActivity(intent);
    }

}
