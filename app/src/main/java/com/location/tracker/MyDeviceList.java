package com.location.tracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.location.tracker.entity.TrackedDevice;
import com.location.tracker.entity.TrackingDevice;
import com.location.tracker.resource.baseResource.DeviceResource;

import java.util.List;

public class MyDeviceList extends ArrayAdapter<TrackedDevice> {

    private List<TrackedDevice> devices;
    private Activity context;
    private TrackingDevice trackingDevice;

    public MyDeviceList(TrackingDevice trackingDevice, Activity context) {
        super(context, R.layout.devices_custom_list, trackingDevice.getDevices());
        this.devices = trackingDevice.getDevices();
        this.trackingDevice = trackingDevice;
        this.context=context;

    }

    private int lastPosition = -1;

    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.devices_custom_list, null,true);

        final TrackedDevice device = devices.get(position);

        TextView tvDeviceID = (TextView) rowView.findViewById(R.id.tvDeviceID);
        TextView tvDeviceName = (TextView) rowView.findViewById(R.id.tvDeviceName);
        final Button btnDelete= (Button) rowView.findViewById(R.id.btnDelete);

        tvDeviceID.setText(device.getId());
        tvDeviceName.setText(device.getName());

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showActionButtons(device);
            }

            private void showActionButtons(final TrackedDevice device) {
                // create an alert builder
                final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);

                // set the custom layout
                final View customLayout = context.getLayoutInflater().inflate(R.layout.device_cliclk_action_buttons, null);

                Button btnUpdateDetails = (Button) customLayout.findViewById(R.id.btn_edit_details);
                Button btnDeleteDevice = (Button) customLayout.findViewById(R.id.btn_delete_device);

                builder.setView(customLayout);
                final android.support.v7.app.AlertDialog dialog = builder.create();

                btnUpdateDetails.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                btnDeleteDevice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDeleteDeviceConfirmAlert(device);
                        dialog.cancel();
                    }
                });

                btnUpdateDetails.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showUpdateDeviceDetailsPopup(device);
                        dialog.cancel();
                    }

                    private void showUpdateDeviceDetailsPopup(TrackedDevice device) {
                        showEditDevicePopup(device);
                        dialog.cancel();
                    }

                    private void showEditDevicePopup(TrackedDevice device) {

                        // create an alert builder
                        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);

                        // set the custom layout
                        final View customLayout = context.getLayoutInflater().inflate(R.layout.device_dialog_view, null);

                        final EditText etTrackingID = (EditText) customLayout.findViewById(R.id.etNewDeviceID);
                        final EditText etTrackingName = (EditText) customLayout.findViewById(R.id.etNewDeviceName);

                        etTrackingID.setEnabled(false);
                        etTrackingID.setText(device.getId());
                        etTrackingName.setText(device.getName());

                        builder.setView(customLayout);
                        // add a button
                        builder.setPositiveButton("Add/update", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                String trackingID = etTrackingID.getText().toString();
                                String name = etTrackingName.getText().toString();
                                TrackedDevice device = new TrackedDevice();
                                device.setId(trackingID);
                                device.setName(name);

                                boolean added = addDevice(trackingDevice, device);
                            }

                            private boolean addDevice(TrackingDevice trackingDevice, TrackedDevice device){
                                MainActivity.deviceResource.addTrackingDevice(trackingDevice, device);
                                return true;
                            }

                        });

                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        // create and show the alert dialog
                        android.support.v7.app.AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                });

                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.show();
            }


            private void showDeleteDeviceConfirmAlert(final TrackedDevice device) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Confirm Delete Device");
                builder.setMessage("Are you sure you want to delete " + device.getName() + "?" );
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    private DeviceResource deviceResource = MainActivity.deviceResource;

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        trackingDevice.getDevices().remove(device);
                        updateTrackingDevice(trackingDevice);
                    }

                    private void updateTrackingDevice(TrackingDevice trackingDevice) {
                        deviceResource.updateTrackingDevice(trackingDevice);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                builder.show();
            }
        });

        return rowView;
    }
}


