package com.location.tracker.entity;

import java.io.Serializable;
import java.util.List;

public class TrackingDevice implements Serializable {

    private String id;

    List<TrackedDevice> devices;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<TrackedDevice> getDevices() {
        return devices;
    }

    public void setDevices(List<TrackedDevice> devices) {
        this.devices = devices;
    }

    @Override
    public String toString() {
        return "TrackingDevice{" +
                "id='" + id + '\'' +
                ", devices=" + devices +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        TrackingDevice trackingDevice = (TrackingDevice) o;
        // field comparison
        return trackingDevice.getId().equals(id);
    }
}