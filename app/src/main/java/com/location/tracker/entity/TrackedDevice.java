package com.location.tracker.entity;

import java.io.Serializable;

public class TrackedDevice implements Serializable {

    String id;

    String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        TrackedDevice trackedDevice = (TrackedDevice) o;
        // field comparison
        return trackedDevice.getId().equals(id);
    }

}