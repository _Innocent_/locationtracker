package com.location.tracker.entity;

import java.io.Serializable;

public class Device implements Serializable {

    private String id;

    private String name;

    private double latitude;

    private double longitude;

    private String status;

    private User user;

    public Device(){}

    public Device(String name, Double latitude, Double longitude) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Device(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double atitude) {
        this.latitude = atitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Device{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;

        if (o instanceof Device){
            Device device = (Device) o;
            if(device.getId().equals(id)){
                return true;
            }
        }

        if(o instanceof  TrackedDevice){
            TrackedDevice device = (TrackedDevice) o;
            if(device.getId().equals(id)){
                return true;
            }
        }

        return false;
    }
}
