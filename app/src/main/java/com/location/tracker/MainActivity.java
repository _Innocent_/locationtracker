package com.location.tracker;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.location.tracker.entity.Device;
import com.location.tracker.entity.User;
import com.location.tracker.resource.DeviceResourceImp;
import com.location.tracker.resource.baseResource.DeviceResource;
import com.location.tracker.services.LocationMonitoringService;
import com.location.tracker.services.notifications.MyFirebaseMessagingService;

import java.util.ArrayList;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    /**
     * Code used in requesting runtime permissions.
     */
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private boolean mAlreadyStartedService = false;
    private TextView mMsgView;
    private TextView mMsgView2;

    //Declarations
    public EditText etDeveiceID;
    public EditText etDeveiceName;
    public static DeviceResource deviceResource = new DeviceResourceImp();
    private static Device device;
    private static boolean navigate;
    private String name;
    private CoordinatorLayout coordinatorLayout;
    private  Snackbar snackbarInternet;

    private static MainActivity context;

    public static MainActivity getContext(){
        return context;
    }
    private Button btnUpdateDetails;
    private Button btnManageDevices;
    private Button btnViewMyLocation;
    private final boolean[] newDevice = {true};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);


        context = this;

        String deviceID = getDeviceID();
        device = new Device();
        device.setId(deviceID);
        populateDevicesFields(deviceID);
        int i;

        //Initializations'
        etDeveiceID = findViewById(R.id.etSecureCode);
        etDeveiceName = findViewById(R.id.etDeviceName);

        mMsgView = (TextView) findViewById(R.id.msgView);
        mMsgView2 = (TextView) findViewById(R.id.msgView2);

        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {

                        if(intent.getAction().equals(LocationMonitoringService.ACTION_LOCATION_BROADCAST)){

                            String latitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LATITUDE);
                            String longitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LONGITUDE);

                            float speed = LocationMonitoringService.speed;
                            float speedAccuracyMetersPerSecond = LocationMonitoringService.speedAccuracyMetersPerSecond;

                            mMsgView2.setText("Seep : " + speed + "\nSpeedAccuracyMetersPerSecond : " + speedAccuracyMetersPerSecond);

                            if (latitude != null && longitude != null) {
                                String locationText = "Latitude : " + latitude + "\n Longitude: " + longitude;
                                mMsgView.setText(locationText);
                            }
                        }else
                            if(intent.getAction().equals(LocationMonitoringService.ACTION_NO_INTERNET_BROADCAST)){
                            showNoInternetPopup();
                        }
                    }

                }, new IntentFilter(LocationMonitoringService.ACTION_LOCATION_BROADCAST)
        );

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showActionOptionButtons();
            }
        });
    }

    public static Device getDevice(){
        return device;
    }
    private void showNoInternetPopup() {
        promptInternetConnect();
    }

    private void showActionOptionButtons() {
        // create an alert builder
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.getContext());

        // set the custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.main_activity_buttons, null);

        btnManageDevices = (Button) customLayout.findViewById(R.id.btn_manage_deveces);
        btnUpdateDetails = (Button) customLayout.findViewById(R.id.btn_update_details);
        btnViewMyLocation = (Button) customLayout.findViewById(R.id.btn_view_my_location);

        if(newDevice[0]){
            btnManageDevices.setEnabled(false);
            btnViewMyLocation.setEnabled(false);
        }else{
            btnManageDevices.setEnabled(true);
            btnViewMyLocation.setEnabled(true);
        }

        builder.setView(customLayout);
        final AlertDialog dialog = builder.create();

        btnViewMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                startActivity(intent);
                dialog.cancel();
            }
        });

        btnManageDevices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DevicesActivity.class);
                startActivity(intent);
                dialog.cancel();
            }
        });

        btnUpdateDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUpdateDeviceDetailsPopup();
                dialog.cancel();
            }
        });

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private void showUpdateDeviceDetailsPopup() {

        // create an alert builder
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.getContext());

        // set the custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.update_device_details_popup, null);

        EditText txtDeveiceID = (EditText) customLayout.findViewById(R.id.txt_device_id);
        EditText txtDeviceName = (EditText) customLayout.findViewById(R.id.txt_device_name);
        EditText txtName = (EditText) customLayout.findViewById(R.id.txt_name);
        EditText txtEmailAddress = (EditText) customLayout.findViewById(R.id.txt_email_address);
        EditText txtPhoneNumber = (EditText) customLayout.findViewById(R.id.txt_phone_num);
        EditText txtSurname = (EditText) customLayout.findViewById(R.id.txt_surname);

        txtDeveiceID.setText(getDeviceID());
        txtDeviceName.setText(device.getName());
        txtName.setText(device.getUser().getName());
        txtSurname.setText(device.getUser().getSurname());
        txtEmailAddress.setText(device.getUser().getEmailAddress());
        txtPhoneNumber.setText(device.getUser().getPhoneNumber());

        builder.setPositiveButton("Add/update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final Device device_ = new Device();
                device_.setId(getDeviceID());
                device_.setName(txtDeviceName.getText().toString());

                User user = new User();
                user.setName(txtName.getText().toString());
                user.setEmailAddress(txtEmailAddress.getText().toString());
                user.setPhoneNumber(txtPhoneNumber.getText().toString());
                user.setSurname(txtSurname.getText().toString());
                device_.setUser(user);
                deviceResource.addDevice(device_);
                dialog.cancel();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setView(customLayout);

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void updateText(final String text){
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMsgView2.setText(text);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        startStep1();
    }

    /**
     * Step 1: Check Google Play services
     */
    private void startStep1() {

        //Check whether this user has installed Google play service which is being used by Location updates.
        if (isGooglePlayServicesAvailable()) {

            //Passing null to indicate that it is executing for the first time.
            startStep2(null);

        } else {
            Toast.makeText(getApplicationContext(), R.string.no_google_playservice_available, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Step 2: Check & Prompt Internet connection
     */
    private Boolean startStep2(DialogInterface dialog) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            //promptInternetConnect();
            return false;
        }

        if (dialog != null) {
            dialog.dismiss();
        }

        //Yes there is active internet connection. Next check Location is granted by user or not.
        if (checkPermissions()) { //Yes permissions are granted by the user. Go to the next step.
            startStep3();
        } else {  //No user has not granted the permissions yet. Request now.
            requestPermissions();
        }
        return true;
    }

    /**
     * Show A Dialog with button to refresh the internet state.
     */
    private void promptInternetConnect() {

        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "No Internet Connection", Snackbar.LENGTH_INDEFINITE)
                .setAction("REFRESH", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (startStep2(null)) {
                            //Now make sure about location permission.
                            if (checkPermissions()) {
                                //Step 2: Start the Location Monitor Service
                                //Everything is there to start the service.
                                startStep3();
                            } else if (!checkPermissions()) {
                                requestPermissions();
                            }
                        }
                    }
                });

        snackbar.show();
    }

    /**
     * Step 3: Start the Location Monitor Service
     */
    private void startStep3() {

        //And it will be keep running until you close the entire application from task manager.
        //This method will executed only once.

        if (!mAlreadyStartedService && mMsgView != null) {

            mMsgView.setText(R.string.msg_location_service_started);

            //Start location sharing service to app server.........
            Intent intent = new Intent(this, LocationMonitoringService.class);
            startService(intent);
            Intent intent2 = new Intent(this, MyFirebaseMessagingService.class);
            startService(intent2);

            mAlreadyStartedService = true;
            //Ends................................................
        }
    }

    /**
     * Return the availability of GooglePlayServices
     */
    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(this, status, 2404).show();
            }
            return false;
        }
        return true;
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState1 = ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionState2 = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        return permissionState1 == PackageManager.PERMISSION_GRANTED && permissionState2 == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Start permissions requests.
     */
    private void requestPermissions() {

        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION);

        boolean shouldProvideRationale2 =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        // Provide an additional rationale to the img_user. This would happen if the img_user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale || shouldProvideRationale2) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_rationale,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the img_user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If img_user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Log.i(TAG, "Permission granted, updates requested, starting location updates");
                startStep3();

            } else {
                // Permission denied.

                // Notify the img_user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the img_user for permission (device policy or "Never ask
                // again" prompts). Therefore, a img_user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation,
                        R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }

    private Device getDevice(String deviceID) {
        return deviceResource.getDevice(deviceID);
    }

    public String getDeviceID() {
        String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        return deviceId;
    }

    private ArrayList<Device> getDevices() {
        return  deviceResource.getDevices();
    }

    public boolean deleteDevice(String deviceId) throws  SecurityException {
        return  deviceResource.deleteDevice(deviceId);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
//            startActivity(intent);
//        }
//
//        if (id == R.id.action_track) {
//            showMyTackDevices(getDeviceID());
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    private void showMyTackDevices(String deviceID) {
        Intent intent = new Intent(getApplicationContext(), DevicesActivity.class);
        intent.putExtra("device", device);
        startActivity(intent);
    }

    private void populateDevicesFields(final String deviceID){

        new DeviceResourceImp().deviceTable.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> snapshotIterator = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = snapshotIterator.iterator();

                if(iterator.hasNext()){

                    while((iterator.hasNext())){

                        Device device_ = iterator.next().getValue(Device.class);

                        if(device_.getId().equals(deviceID)){
                            device = device_;
                            newDevice[0] = false;
                        }
                    }
                }

                if(newDevice[0]){
                    device = new Device();
                    device.setId(deviceID);
                    device.setUser(new User());
                }

                etDeveiceID.setText(convertNullToString(device.getId()));
                etDeveiceName.setText(convertNullToString(device.getName()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private String convertNullToString(String text){
        if(text == null){
            text = "";
        }
        return text;
    }
}
