package com.location.tracker.directionhelpers;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
