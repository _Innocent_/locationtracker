package com.location.tracker;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.location.tracker.entity.Device;
import com.location.tracker.entity.TrackingDevice;
import com.location.tracker.entity.User;
import com.location.tracker.services.notifications.MyFirebaseMessagingService;
import com.location.tracker.services.notifications.MySingleton;
import com.location.tracker.resource.DeviceResourceImp;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

public class SplashActivity extends AppCompatActivity{

    private static final int PERMISSION_REQUEST_CODE = 1;

    public static SplashActivity activity;
    public static Context context;

    public static Device device;
    public static Map<String, Device> devices = new HashMap<>();
    public static Map<String, TrackingDevice> trackingDevices = new HashMap<>();
    private boolean navigated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        activity = this;
        context = getApplicationContext();

        Intent intent2 = new Intent(this, MyFirebaseMessagingService.class);
        startService(intent2);

        //doNotification();

        getTrackingDevices();
    }



























    public String getDeviceID() {
        String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        return deviceId;
    }

    private void getTrackingDevices() {
        new DeviceResourceImp().trackingDeviceTable.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> snapshotIterator = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = snapshotIterator.iterator();

                while((iterator.hasNext())){

                    TrackingDevice trackingDevice = iterator.next().getValue(TrackingDevice.class);
                    trackingDevices.put(trackingDevice.getId(), trackingDevice);
                }

                getDevice(getDeviceID());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void getDevice(final String deviceID){

        final boolean[] newDevice = {true};

        new DeviceResourceImp().deviceTable.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> snapshotIterator = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = snapshotIterator.iterator();

                if (iterator.hasNext()) {

                    while ((iterator.hasNext())) {

                        Device device_ = iterator.next().getValue(Device.class);
                        devices.put(device_.getId(), device_);

                        if (device_.getId().equals(deviceID)) {
                            device = device_;
                            newDevice[0] = false;
                        }
                    }
                }

                if (newDevice[0]) {
                    device = new Device();
                    device.setId(deviceID);
                    device.setUser(new User());
                }

                if(!navigated){
                    navigated = true;
                    startSplash();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void startSplash(){
        int min = 3000;
        int max = 3000;

        Random randomNum = new Random();
        int SPLASH_DISPLAY_LENGTH = 20;
//        int SPLASH_DISPLAY_LENGTH = min + randomNum.nextInt(max);;

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    public void sendSMS(){

        if (checkPermission()) {
            Log.e("permission", "Permission already granted.");
        } else {
            requestPermission();
        }

        String no = "0659674970";
        String msg = "testing sms";

        Intent intent=new Intent(getApplicationContext(),SplashActivity.class);
        PendingIntent pi=PendingIntent.getActivity(getApplicationContext(), 0, intent,0);

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(no, null, msg, pi,null);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, PERMISSION_REQUEST_CODE);

    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.SEND_SMS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
}
