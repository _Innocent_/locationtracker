package com.location.tracker.resource.baseResource;


import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public abstract class CoreDeviceResource {

    private FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

    private DatabaseReference rootReference = firebaseDatabase.getReference();

    //Tables
    public DatabaseReference deviceTable = rootReference.child("device");
    public DatabaseReference trackingDeviceTable = rootReference.child("tracking_device");
}
