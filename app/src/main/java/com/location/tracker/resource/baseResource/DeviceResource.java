package com.location.tracker.resource.baseResource;

import com.location.tracker.entity.Device;
import com.location.tracker.entity.TrackedDevice;
import com.location.tracker.entity.TrackingDevice;

import java.util.ArrayList;

public interface DeviceResource {

    public void addDevice(Device device);

    public ArrayList<Device> getDevices();

    public boolean deleteDevice(String id);

    public boolean updateDevice(Device device);

    public Device getDevice(String deviceId);

    public boolean addTrackingDevice(TrackingDevice trackingDevice, TrackedDevice newDevice);

    public void updateTrackingDevice(TrackingDevice trackingDevice);
}
