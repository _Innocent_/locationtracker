package com.location.tracker.resource;


import com.location.tracker.entity.Device;
import com.location.tracker.entity.TrackedDevice;
import com.location.tracker.entity.TrackingDevice;
import com.location.tracker.resource.baseResource.CoreDeviceResource;
import com.location.tracker.resource.baseResource.DeviceResource;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DeviceResourceImp extends CoreDeviceResource implements DeviceResource {

    public void addDevice(Device device) {
        deviceTable.child(device.getId()).setValue(device);
    }
    private final ArrayList<Device> devices = new ArrayList<>();

    public boolean deleteDevice(String id) {
        //getting the specified User reference
        DatabaseReference DeleteReference = FirebaseDatabase.getInstance().getReference("device").child(id);
        //removing User
        DeleteReference.removeValue();

        return true;
    }

    public boolean updateDevice(Device device) {
        //getting the specified User reference
        DatabaseReference UpdateReference = FirebaseDatabase.getInstance().getReference("device").child(device.getId());
        //update  User  to firebase
        UpdateReference.setValue(device);

        return true;
    }

    Device device = null;

    public Device getDevice(final String deviceId) {

        populateDevicesList();

        return getSpecificDevice(deviceId);
    }

    private Device getSpecificDevice(String deviceID) {
        Device device = null;
        for(Device d : devices){
            if(device.getId().equals(deviceID)){
                device = d;
            }
        }
        return device;
    }

    private void populateDevicesList(){
        deviceTable.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> snapshotIterator = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = snapshotIterator.iterator();

                while((iterator.hasNext())){
                    Device device = iterator.next().getValue(Device.class);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public ArrayList<Device> getDevices() {

        final ArrayList<Device> devices = new ArrayList<>();

        deviceTable.child("device").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //clearing the previous User list
                devices.clear();
                //getting all nodes
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    //getting User from firebase console
                    Device device = postSnapshot.getValue(Device.class);
                    //adding User to the list
                    devices.add(device);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return devices;
    }

    public boolean addTrackingDevice(final TrackingDevice trackingDevice, final TrackedDevice newDevice){

        final boolean[] found = {false};

        deviceTable.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> snapshotIterator = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = snapshotIterator.iterator();

                while((iterator.hasNext())){

                    TrackedDevice device = iterator.next().getValue(TrackedDevice.class);

                    if(device.getId().equals(newDevice.getId())){

                        if(trackingDevice.getDevices() == null){
                            trackingDevice.setDevices(new ArrayList<TrackedDevice>());
                        }

                        if(!trackingDevice.getDevices().contains(device)){
                            trackingDevice.getDevices().add(newDevice);
                        }else{
                            trackingDevice.getDevices().get(trackingDevice.getDevices().indexOf(newDevice)).setName(newDevice.getName());
                        }

                        updateTrackingDevice(trackingDevice);
                        found[0] = true;
                        break;
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return found[0];
    }

    public void updateTrackingDevice(TrackingDevice trackingDevice) {
        //getting the specified User reference
        DatabaseReference UpdateReference = FirebaseDatabase.getInstance().getReference("tracking_device").child(trackingDevice.getId());
        //update  User  to firebase
        UpdateReference.setValue(trackingDevice);
    }

    private List<Device> getAllDevices(){
        final List<Device> devices = new ArrayList<>();
        deviceTable.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> snapshotIterator = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = snapshotIterator.iterator();

                while((iterator.hasNext())){
                    Device device = iterator.next().getValue(Device.class);
                    devices.add(device);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return devices;
    }
}
