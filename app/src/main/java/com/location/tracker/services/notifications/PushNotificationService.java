package com.location.tracker.services.notifications;

import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.location.tracker.SplashActivity;
import com.location.tracker.entity.Device;
import com.location.tracker.services.battery.utility.ServiceUtils;
import com.location.tracker.services.mail.MailerService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.location.tracker.SplashActivity.context;

public class PushNotificationService {

    private Device device;

    final private String FCM_API = "https://fcm.googleapis.com/fcm/send";
    final private String serverKey = "key=" + "AIzaSyCy_98OyT443yTykDRelHTIF15mVkdQeKc";
    final private String contentType = "application/json";
    final String TAG = "NOTIFICATION TAG";

    private static PushNotificationService instance;

    private PushNotificationService(){
        this.device = SplashActivity.device;
    }

    public static PushNotificationService getInstance(){
        if(instance == null){
            instance = new PushNotificationService();
        }
        return instance;
    }

    public void sendNotification(String message, String title) {

        device = SplashActivity.device;

        Log.d("","Sendong Nofification **************************************************");
//      String TOPIC = "/topics/"+getDeviceID(); //topic has to match what the receiver subscribed to
        String TOPIC = "/topics/news"; //topic has to match what the receiver subscribed to

        JSONObject notification = new JSONObject();
        JSONObject notifcationBody = new JSONObject();
        try {

            notifcationBody.put("title", ServiceUtils.replaceTemplateData(title));
            notifcationBody.put("message", ServiceUtils.replaceTemplateData(message));

            notification.put("to", TOPIC);
            notification.put("data", notifcationBody);
        } catch (JSONException e) {
            Log.e(TAG, "onCreate: " + e.getMessage() );
        }
        sendNotification(notification);
    }

    private void sendNotification(JSONObject notification) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(FCM_API, notification,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "onResponse: " + response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Request error", Toast.LENGTH_LONG).show();
                        Log.i(TAG, "onErrorResponse: Didn't work");
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", serverKey);
                params.put("Content-Type", contentType);
                return params;
            }
        };
        MySingleton.getInstance(context.getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

}
