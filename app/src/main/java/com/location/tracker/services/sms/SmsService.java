package com.location.tracker.services.sms;

import com.nexmo.client.NexmoClient;
import com.nexmo.client.NexmoClientException;
import com.nexmo.client.sms.SmsSubmissionResponse;
import com.nexmo.client.sms.SmsSubmissionResponseMessage;
import com.nexmo.client.sms.messages.TextMessage;

import java.io.IOException;

public class SmsService {

    public void sendSMS(){
        SmsSubmissionResponse response = null;
        try {
            NexmoClient client = new NexmoClient.Builder()
                    .apiKey("ba82f50d")
                    .apiSecret("N8nG8OiNuaM7oBtL")
                    .build();
            String messageText = "Hello from Nexmo";
            TextMessage message = new TextMessage("Location Tracker", "27659674970", messageText);

            response = client.getSmsClient().submitMessage(message);

            for (SmsSubmissionResponseMessage responseMessage : response.getMessages()) {
                System.out.println(responseMessage);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NexmoClientException e) {
            e.printStackTrace();
        }
    }
}
