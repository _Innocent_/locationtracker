package com.location.tracker.services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.location.tracker.MainActivity;
import com.location.tracker.R;
import com.location.tracker.entity.Device;
import com.location.tracker.entity.User;
import com.location.tracker.resource.DeviceResourceImp;
import com.location.tracker.services.battery.BatteryService;

import java.util.Iterator;


public class LocationMonitoringService extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final String TAG = LocationMonitoringService.class.getSimpleName();
    GoogleApiClient mLocationClient;
    LocationRequest mLocationRequest = new LocationRequest();

    private Device device = null;

    public static final String ACTION_LOCATION_BROADCAST = LocationMonitoringService.class.getName() + "LocationBroadcast";
    public static final String ACTION_NO_INTERNET_BROADCAST = LocationMonitoringService.class.getName() + "LocationBroadcast";

    public static final String EXTRA_LATITUDE = "extra_latitude";
    public static final String EXTRA_LONGITUDE = "extra_longitude";

    private static double prevLatitude = 0.0;
    private static double prevLongitude = 0.0;

    @SuppressLint("NewApi")
    @Override
    public void onCreate() {

        BatteryService batteryService = new BatteryService();

        NotificationManager notificationManager = (NotificationManager) getSystemService(this.NOTIFICATION_SERVICE);
        String channelId = getString(R.string.app_name);
        NotificationChannel notificationChannel = new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel.setDescription(channelId);
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(Color.GREEN);
        notificationChannel.shouldShowLights();

        notificationManager.createNotificationChannel(notificationChannel);
        Notification notification = new Notification.Builder(this, channelId)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("This device is being tracked")
                .setSmallIcon(R.drawable.icons_notification)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setLights(Color.GREEN, 1000, 1000)
                .setChronometerCountDown(true)
                .setColor(Color.BLUE)
                .build();
        startForeground(111, notification);

        new BatteryService();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mLocationClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        int interval = 1000;
        mLocationRequest.setInterval(interval);
        mLocationRequest.setFastestInterval(interval/2);
        //mLocationRequest.setSmallestDisplacement(0.5f);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationClient.connect();

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /*
     * LOCATION CALLBACKS
     */
    @Override
    public void onConnected(Bundle dataBundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            //Log.d(TAG, "== Error On onConnected() Permission not granted ___________________________________________________" );
            //Permission not granted by user so cancel the further execution.

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);

        //Log.d(TAG, "Connected to Google API ____________________________________________________________________________");
    }

    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Connection suspended ____________________________________________________________");
    }

    public static float speedAccuracyMetersPerSecond;
    public static float speed;

    //to get the location change
    @SuppressLint("NewApi")
    @Override
    public void onLocationChanged(Location location) {

        if (location != null && location.getLatitude() != LocationMonitoringService.prevLatitude && location.getLongitude() != LocationMonitoringService.prevLongitude) {

            /**
             * By default speed is returned in meters per sec and now by dividing it by 1000
             * returns the speed in KM per sec
             */
            speedAccuracyMetersPerSecond = location.getSpeedAccuracyMetersPerSecond() / 1000;
            speed = location.getSpeed() / 1000;

            //Log.d(TAG, "Location changed ____________________________________________________________________");
            LocationMonitoringService.prevLongitude = location.getLongitude();
            LocationMonitoringService.prevLatitude = location.getLatitude();

            //Send result to activities
            sendMessageToUI(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
            updateDevice(location);
        }
//        Toast.makeText(getApplicationContext(),"Location tracking", Toast.LENGTH_SHORT).show();
        //Log.d(TA0G, new Date().getSeconds() + " Tracking Location ___________________________________________________" );
    }

    private boolean checkPermissions() {
        int permissionState1 = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionState2 = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        return permissionState1 == PackageManager.PERMISSION_GRANTED && permissionState2 == PackageManager.PERMISSION_GRANTED;

    }

    private void sendMessageToUI(String lat, String lng) {
        //Log.d(TAG, "Sending info... ________________________________________________________________");
        Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
        intent.putExtra(EXTRA_LATITUDE, lat);
        intent.putExtra(EXTRA_LONGITUDE, lng);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendNoInternetToUI() {
        Log.d(TAG,"Sendin g no internet message");
        Toast.makeText(getApplicationContext(), "Sendin g no internet message",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(ACTION_NO_INTERNET_BROADCAST);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void updateDevice(final Location location){

        final boolean[] deviceFound = {false};

        if(device == null){
            final String deviceID = getDeviceID();
            final DatabaseReference deviceTable = new DeviceResourceImp().deviceTable;
            deviceTable.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    Iterable<DataSnapshot> snapshotIterator = dataSnapshot.getChildren();
                    Iterator<DataSnapshot> iterator = snapshotIterator.iterator();

                    while ((iterator.hasNext())) {

                        Device device_ = iterator.next().getValue(Device.class);
                        if (device_.getId().equals(deviceID)) {
                            device_.setLatitude(location.getLatitude());
                            device_.setLongitude(location.getLongitude());
                            device = device_;
                            MainActivity.deviceResource.updateDevice(device);
                            deviceFound[0] = true;
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }else{
            device.setLatitude(location.getLatitude());
            device.setLongitude(location.getLongitude());
            MainActivity.deviceResource.updateDevice(device);
        }
    }

    public String getDeviceID() {
        String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        return deviceId;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "Failed to connect to Google API _______________________________________________________________________");
    }

}