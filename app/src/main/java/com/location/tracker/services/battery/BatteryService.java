package com.location.tracker.services.battery;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.os.BatteryManager;

import com.location.tracker.SplashActivity;
import com.location.tracker.services.constants.MessageType;
import com.location.tracker.services.mail.MailerService;
import com.location.tracker.services.notifications.PushNotificationService;
import com.nexmo.client.sms.callback.messages.MO;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

public class BatteryService{

    private boolean deviceCharging;
    private boolean usbCharging;
    private boolean acCharging;
    private int batteryLevel;
    private int chargePluged;
    private Context context;
    private Activity activity;

    public BatteryService(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
        startMonitoringBattery();
    }

    public BatteryService() {
        this.context = SplashActivity.context;
        this.activity = SplashActivity.activity;
        startMonitoringBattery();
    }

    /*
    * This method will run every specified interval monitoring the battery level
    * */
    private void startMonitoringBattery() {
        Timer myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                checkBatteryStatus();
            }
        }, 0, 1000);
    }

    private void checkBatteryStatus(){

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        deviceCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

        chargePluged = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        usbCharging = chargePluged == BatteryManager.BATTERY_PLUGGED_USB;
        acCharging = chargePluged == BatteryManager.BATTERY_PLUGGED_AC;
        batteryLevel = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);

        broadcastCommunications();
    }

    private boolean prevDeveicCharging;
    private boolean levelDroppedNotificationSend;
    private int prevBatteryLevel = batteryLevel;

    private void broadcastCommunications(){

        String message = null;
        String title = null;
//        String message = getMessage(MessageType.TESTING);
//        String title = MessageType.TESTING.name();

        if(deviceCharging && !prevDeveicCharging){
            //send email and sms device is now charging...
            prevDeveicCharging = deviceCharging;
            message = getMessage(MessageType.CHARGER_CONNECTED);
            title = MessageType.CHARGER_CONNECTED.name();
        }

        if(!deviceCharging && prevDeveicCharging){
            //send email and sms device stopped charging...
            prevDeveicCharging = deviceCharging;
            message = getMessage(MessageType.CHARGER_DISCONNECTED);
            title = MessageType.CHARGER_DISCONNECTED.name();
        }

        if(batteryLevel % 10 == 0 && !deviceCharging && !levelDroppedNotificationSend && prevBatteryLevel != batteryLevel && batteryLevel != 100){
            levelDroppedNotificationSend = true;
            prevBatteryLevel = batteryLevel;
            //send email and sms not charging and battery has fallen by 10%....
            message = getMessage(MessageType.BATTERY_LEVEL_DROPPED);
            title = MessageType.BATTERY_LEVEL_DROPPED.name();
        }

        if(batteryLevel % 10 != 0){
            levelDroppedNotificationSend = false;
        }

        if(message != null && title != null) {
           // MailerService.getInstance().sendEmail(message, title);
            PushNotificationService.getInstance().sendNotification(message, title);
        }
    }

    private String getMessage(MessageType type) {
        String meassage = "";
        try {

            Properties properties = new Properties();
            AssetManager assetManager = context.getAssets();
            InputStream inputStream = assetManager.open("messages.properties");
            properties.load(inputStream);

            meassage = properties.getProperty(type.name());

        }catch (IOException e){
            e.fillInStackTrace();
        }
        return meassage;
    }
}