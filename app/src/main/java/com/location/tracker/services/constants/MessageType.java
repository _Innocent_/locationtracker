package com.location.tracker.services.constants;

import java.util.Map;

public enum MessageType {
    CHARGER_DISCONNECTED,
    CHARGER_CONNECTED,
    BATTERY_LEVEL_DROPPED,
    TESTING;
}
