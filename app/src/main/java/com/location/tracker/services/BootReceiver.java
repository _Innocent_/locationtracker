package com.location.tracker.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.location.tracker.services.LocationMonitoringService;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
            Log.d("Boot","Started on Boot_________________________________________________________") ;
            Intent serviceIntent = new Intent(context, LocationMonitoringService.class);
            context.startService(serviceIntent);
        }
    }
}
