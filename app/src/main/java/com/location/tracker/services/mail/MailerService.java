package com.location.tracker.services.mail;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import android.content.res.AssetManager;
import android.util.Log;

import com.location.tracker.SplashActivity;
import com.location.tracker.entity.Device;
import com.location.tracker.entity.TrackedDevice;
import com.location.tracker.entity.TrackingDevice;
import com.location.tracker.services.constants.MessageType;

import static com.location.tracker.SplashActivity.context;

public final class MailerService {

    private Device device;
    private Map<String, Device> devices;
    private Map<String, TrackingDevice> trackingDevices;

    private static MailerService instance;

    private MailerService(){
        Log.d("","Created mailer Service **************************************************");
        this.device = SplashActivity.device;
        this.devices = SplashActivity.devices;
        this.trackingDevices = SplashActivity.trackingDevices;
    }

    public static MailerService getInstance(){
        if(instance == null){
            instance = new MailerService();
        }
        return instance;
    }

    public void sendEmail(String messageText, String subj) {

        this.device = SplashActivity.device;
        this.devices = SplashActivity.devices;
        this.trackingDevices = SplashActivity.trackingDevices;

        String subjectText = "Location Tracker " + subj;

        messageText = replaceTemplateData(messageText);
        subjectText = replaceTemplateData(subjectText).toUpperCase();

        System.out.println(messageText);

        final String username = "innocent.javas@gmail.com";
        final String password = "Javas@19";

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));

            for (String recipient:getRecipients()) {
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(recipient));
            }

            message.setSubject(subjectText);
            message.setText(messageText);

            //Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    /*
    * Replace the template data with actual data
    * */
    private String replaceTemplateData(String text) {

        text = text.replace("$user", device.getUser().getName() + " " + device.getUser().getSurname());
        text = text.replace("_", " ");

        return text;
    }

    private Set<String> getRecipients(){

        Set<String> recipientDevices = new HashSet<>();
        Set<String> recipientEmails = new HashSet<>();

        for (String key :trackingDevices.keySet()) {
            for (TrackedDevice trackedDevice:trackingDevices.get(key).getDevices()) {
                if(trackedDevice.getId().equals(device.getId())){
                    recipientDevices.add(trackingDevices.get(key).getId());
                }
            }
        }

        for (String key:devices.keySet()) {
            if(recipientDevices.contains(this.device.getId())){
                recipientEmails.add(devices.get(key).getUser().getEmailAddress());
            }
        }

        return recipientEmails;
    }

}
