package com.location.tracker.services.battery.utility;

import com.location.tracker.SplashActivity;
import com.location.tracker.entity.Device;
import com.location.tracker.entity.User;

public class ServiceUtils {

    private static Device device;

    static {
        device = SplashActivity.device;
    }

    public static String replaceTemplateData(String text) {

        device = SplashActivity.device;

        text = text.replace("$user", device.getUser().getName() + " " + device.getUser().getSurname());
        text = text.replace("$DeviceName", device.getName());
        text = text.replace("_", " ");

        return text;
    }
}
