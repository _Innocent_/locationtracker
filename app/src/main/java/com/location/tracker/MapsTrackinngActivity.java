package com.location.tracker;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.location.tracker.directionhelpers.FetchURL;
import com.location.tracker.directionhelpers.TaskLoadedCallback;
import com.location.tracker.entity.Device;
import com.location.tracker.entity.TrackedDevice;
import com.location.tracker.entity.TrackingDevice;
import com.location.tracker.resource.DeviceResourceImp;
import com.location.tracker.services.LocationMonitoringService;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MapsTrackinngActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, TaskLoadedCallback {

    private static final String TAG = MapsTrackinngActivity.class.getSimpleName();
    private GoogleMap mMap;
    private TrackingDevice trackingDevice;
    private Map<String, TrackedDevice> devices = new HashMap<>();
    private LatLng originLatLng;
    private LatLng destinationLatLng;
    private static MapsTrackinngActivity context;
    private boolean trackingLocation;
    private Map<String, Marker> markers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_trackinng);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        trackingDevice = (TrackingDevice) intent.getSerializableExtra("trackingDevice");

        context = this;
        markers = new HashMap<>();

        readDeviceLocations();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String str_latitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LATITUDE);
                        String str_longitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LONGITUDE);

                        if (str_latitude != null && str_longitude != null) {
                            double latitude = Double.valueOf(str_latitude);
                            double longitude = Double.valueOf(str_longitude);

                            originLatLng = new LatLng(latitude, longitude);
                        }
                    }

                }, new IntentFilter(LocationMonitoringService.ACTION_LOCATION_BROADCAST)
        );
    }

    public static MapsTrackinngActivity getContext(){
        return context;
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        Toast.makeText(getApplicationContext(), "Marker clicked", Toast.LENGTH_SHORT).show();
        for (String key: markers.keySet()) {
            if(markers.get(key).equals(marker)){
                return true;
            }
        }
        return false;
    }

    private void readDeviceLocations() {
        final DatabaseReference deviceTable = new DeviceResourceImp().deviceTable;
        deviceTable.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> snapshotIterator = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = snapshotIterator.iterator();

                while ((iterator.hasNext())) {

                    Device device = iterator.next().getValue(Device.class);

                    if (trackingDevice.getDevices().contains(device)) {
                        printMarkerOnMap(device);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void printMarkerOnMap(Device device) {

//        double latitude = device.getLatitude();
//        double longitude = device.getLongitude();
        double latitude = -28.2;
        double longitude = 26.5;

        LatLng latLng = new LatLng(latitude, longitude);

        MarkerOptions marker = new MarkerOptions().position(latLng).title(device.getName());

        if (markers.get(device.getId()) != null) {
            markers.get(device.getId()).remove();
        }

        Marker locatiioonMarker = mMap.addMarker(marker);
        markers.put(device.getId(), locatiioonMarker);
        markers.get(device.getId()).setTag(latLng);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mMap.setMyLocationEnabled(true);

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                LatLng destinationLatLng = (LatLng)marker.getTag();
                Toast.makeText(getApplicationContext(), "Marker clicked " + destinationLatLng.latitude + "  " + destinationLatLng.longitude, Toast.LENGTH_SHORT).show();

                place1 = new MarkerOptions().position(new LatLng(mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude())).title("Location 1");
                place2 = new MarkerOptions().position(destinationLatLng).title("Location 2");
                new FetchURL(MapsTrackinngActivity.this).execute(getUrl(place1.getPosition(), place2.getPosition(), "driving"), "driving");
                trackingLocation = true;
                return false;
            }
        });
    }
    private MarkerOptions place1, place2;
    private Polyline currentPolyline;

    private String getUrl(LatLng origin, LatLng dest, String directionMode) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Mode
        String mode = "mode=" + directionMode;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + getString(R.string.google_maps_key);
        return url;
    }

    @Override
    public void onTaskDone(Object... values) {
        if (currentPolyline != null)
            currentPolyline.remove();
        currentPolyline = mMap.addPolyline((PolylineOptions) values[0]);
    }

}
